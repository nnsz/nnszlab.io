# NNSZ指南README


## 介绍

此项目用于托管NNSZ生活指南的网站
项目所有权为南宁市第三中学全体师生
欢迎所有人来为这个项目做出贡献

## 投稿

联系南宁三中万能墙 or admin@novashen.top

## SOP

- 对于每一条提交，需要审核其内容合法性
- 对于每一条内容，管理者判断其若其失去时效性，应用删除线包裹内容，并注明失效修改时间，对于原有正确内容，尽量不删除
- 对于每一条提交，管理者合并前需要在其内容下方标注贡献者及贡献时间，以便读者判断其时效性
- 对于每一次合并，管理者需要审核其无冲突方可允许，管理者需要按照`更新方法`的指引进行项目更新
- 对于每一个页面，创建者需要在head中引入js文件夹中的所有资源
- 对于每一个贡献者，管理者需要将其加入readme和index的致谢中，记录其联系邮箱

## 更新方法

对于每一次更新，管理者需要：

1. 从项目中获取最新的md文件

2. 按照SOP的标准进行内容的增改，并用typora导出为HTML(without styles)并命名为index.html

3. 用文本编辑器打开，并在`<head>`与`</head>`中间加入

   ```html
   <script type="text/javascript" src="js/highlight.min.js"></script>
   <script type="text/javascript" src="js/markdownPad2AutoCatalog.min.js"></script>
   <link rel="stylesheet" type="text/css" href="js/markdownPad2AutoCatalog.min.css">
   ```

4. 检查冲突并使用git提交，若不会用git，请看5

5. 在gitlab中将项目中原有的index.html删除，并上传新的index.html

## 关于Typora

[Typora_1.1.4_423down.com.7z ](https://lanzoux.com/ineD101p5ubi)

## 支持

长期技术支持 admin@novashen.top

内容支持 待定

## 贡献者与致谢

项目创建者 admin@novashen.top

技术支持 @cayxc

## TODO

1. 找到负责的网站管理者
2. 建立作者群，征集意见，形成初步的管理方案
3. 在假期前修编完成手册第一版，供招生使用
4. 优化更新方法
5. 提供更方便的内容管理系统

## 许可

Apache License 2.0

## 项目状态

2023.3.6 创建项目

2023.3.9 修订readme，新增TODO